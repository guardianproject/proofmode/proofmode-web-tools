import micropip


async def load_libraries():
    await micropip.install('matplotlib')
    await micropip.install('PyPDF2')
    await micropip.install('altair')
    await micropip.install('pandas')

load_libraries()
