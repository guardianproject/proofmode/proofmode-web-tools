import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
from PyPDF2 import PdfFileMerger
from matplotlib import rcParams
import altair as alt
import pandas as pd
from io import StringIO
from proofcheck import proofData

proof_data = StringIO(proofData)

df = pd.read_csv(proof_data)
updated_df = df.rename(columns={'File Hash SHA256': 'File_Hash_SHA256', 'Location.Provider': 'Location_Provider', 'Location.Accuracy': 'Location_Accuracy', 'Location.Latitude': 'Location_Latitude', 'Wifi MAC': 'Wifi_MAC', 'Location.Longitude': 'Location_Longitude',
                       'Location.Bearing': 'Location_Bearing', 'Location.Time': 'Location_Time', 'File Modified': 'File_Modified', 'Location.Altitude': 'Location_Altitude', 'Proof Generated': 'Proof_Generated', 'File Path': 'File_Path', 'Location.Speed': 'Location_Speed'})

print(df)

hardware_software_metadata_list = ['Locale', 'Location_Provider', 'IPv6', 'IPv4', 'Language',
                                   'NetworkType', 'Network', 'Manufacturer', 'DataType', 'Hardware', 'ScreenSize', 'CellInfo']

# Initiate device ID dataframe
DeviceID_List = []

for i in list(updated_df['DeviceID']):
    if i not in DeviceID_List:
        DeviceID_List.append(i)

device_df = pd.DataFrame(columns=DeviceID_List,
                         index=hardware_software_metadata_list)

for i, row in device_df.iterrows():
    aggregate_df = updated_df.groupby("DeviceID")[i].nunique()
    device_df = device_df.append(aggregate_df)
print(device_df)

device_df = device_df.dropna()
device_df.head()
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(device_df)

transpose_df = device_df.T

with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(transpose_df)

source = transpose_df

chart = alt.Chart(source).transform_fold(
    hardware_software_metadata_list,
    as_=['Device Attribute', 'Unique Devices']
).mark_bar(
    opacity=.5,
    binSpacing=1,
    clip=True
).encode(
    x=alt.X('Unique Devices:Q', axis=alt.Axis(
        title='Number of Unique Attributes per Device', tickMinStep=1)),
    y=alt.Y('count()', stack=None, axis=alt.Axis(
        title='Number of Devices', tickMinStep=1)),
    color=alt.Color('Device Attribute:N', scale=alt.Scale(scheme='paired'))

).properties(
    title='Histogram of Device Attributes Across Devices'
)

chart.configure_title(
    fontSize=20,
    font='Tahoma',
    anchor='start',
    color='grey')


output = chart.to_json()
output
